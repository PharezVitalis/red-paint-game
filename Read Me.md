


Sprite Sources:
Paintbrush :http://www.clipartpanda.com/clipart_images/artist-s-paint-brush-4412314
Paintball Gun : https://openclipart.org/detail/264397/gun-5
Wolf: https://pixabay.com/en/wolf-animal-canine-cartoon-1295231/
Rabbit: https://pixabay.com/en/rabbit-bunny-cute-animal-ears-1283940/
Donkey: https://pixabay.com/en/donkey-animal-farm-gray-comic-310798/
Tree 1 : https://pixabay.com/en/tree-forest-trunk-nature-leaves-576847/
Tree 2: https://pixabay.com/en/tree-bush-clipart-nature-forest-1898040/
Cloud 1 : http://www.publicdomainpictures.net/view-image.php?image=181227&picture=cloud-40
Cloud 2 : http://www.publicdomainpictures.net/view-image.php?image=181194&picture=cloud-10
Sun : http://www.publicdomainpictures.net/view-image.php?image=181319&picture=the-sun-4
car: https://pixabay.com/en/trabant-car-transport-white-drive-782799/
petrol pump: https://pixabay.com/en/gas-pump-gas-station-petrol-station-297117/

Sound Sources:
Paintball Gun: https://www.freesound.org/people/Michaelvelo/sounds/366835/
Stage Start: https://www.freesound.org/people/bluebloomers/sounds/202901/
Forest Theme: http://freemusicarchive.org/music/The_Owl/Fairy_Forest/the_owl_-_fairy_forest_-_04_the_owl_-_telescope
UI Feedback: https://www.freesound.org/people/dland/sounds/320181/
Ice Walk: https://www.freesound.org/people/JarredGibb/sounds/263910/
Leaf Walk: https://www.freesound.org/people/CGEffex/sounds/85653/
Ice Theme: http://freemusicarchive.org/music/Arthur_Zdrinc/Its_AD_2016/Arthur_Zdrinc_-_Its_AD_2016_-_07_The_Winter_of_1984
New Wave: https://www.freesound.org/people/jmayoff/sounds/256257/
UI Theme: http://freemusicarchive.org/music/The_Joy_Drops/Not_Drunk_EP/NotDrunk-mix-full-band-no-vocal
Forest Ambience: https://www.freesound.org/people/Kyster/sounds/97178/
Ice Ambience: https://www.freesound.org/people/Bosk1/sounds/144083/
PaintBrush Swish: https://freesound.org/people/jawbutch/sounds/344408/
PaintBrush Hit: https://freesound.org/people/Benboncan/sounds/147541/