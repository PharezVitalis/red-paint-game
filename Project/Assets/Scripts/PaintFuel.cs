﻿using UnityEngine;
using System.Collections;

public class PaintFuel : MonoBehaviour
{
    [SerializeField]
    private float paintGainRate = 15;
    [SerializeField]
    private ParticleSystem partSys;
    int playersInBox = 0;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            playersInBox++;
            partSys.enableEmission = true;
        }
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            col.GetComponent<PlayerController>().ChangeInPaint(paintGainRate * Time.deltaTime);
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            playersInBox--;
            if (playersInBox < 1)
            {
                partSys.enableEmission = false;
            }
        }

      
    }
}
	
