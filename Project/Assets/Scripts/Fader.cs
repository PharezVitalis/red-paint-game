﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour {
    SpriteRenderer rend;
    [SerializeField]
    float fadeTime = 0.3f;
    [SerializeField]
    float fadeValue = 0.85f;
    float cAlpha = 1;

    int charactersColliding = 0;

	// Use this for initialization
	void Start () {
        rend = GetComponent<SpriteRenderer>();
	}
	
	void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player" || col.gameObject.tag == "NPC")
        {
            charactersColliding++;

            StartCoroutine(Fade());
        }
    }

    void OnTriggerExit2D (Collider2D col)
    {
        if (col.tag == "Player" || col.gameObject.tag == "NPC")
        {
            charactersColliding--;

        }
    }

    IEnumerator Fade()
    {
        while (cAlpha > fadeValue)
        {
            cAlpha -= (1 - fadeValue) * Time.deltaTime*(1/fadeTime);
            rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, cAlpha);

            yield return new WaitForFixedUpdate();
        }
        yield return new WaitWhile(() => charactersColliding > 0);

        while (cAlpha < 1)
        {
             cAlpha += (1 - fadeValue) * Time.deltaTime * (1 / fadeTime);
            rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, cAlpha);

            yield return new WaitForFixedUpdate();
        }
    }

    
}


