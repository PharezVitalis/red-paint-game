﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarBehaviour : EnemyController {
    Rigidbody2D rbody;

    [SerializeField]
    float damage = 29; 

    [SerializeField]
    LayerMask Ground;
    [SerializeField]
    float forceMag = 400;

    [SerializeField]
    private float speed = 15;
    [SerializeField]
    private float freezeTime = 0.7f;
    float xDir = -1;
    bool isFrozen = false;
   

  
	
    
 

    void OnEnable()
    {
        if (!rbody) rbody = GetComponent<Rigidbody2D>();
        rbody.velocity = Vector2.right * xDir * speed;
        InvokeRepeating("CheckForEdge", 0, freezeTime);
    }

    void CheckForEdge()
    {
        

        if (isFrozen) return;
     
       

       
       if (rbody.velocity.magnitude < 1)
        {
            ResetWithNewDirection();
        }
    }

    void ResetWithNewDirection()
    {
        xDir *= -1;
        isFrozen = true;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);

        rbody.velocity = Vector2.zero;

        Invoke("UnFreeze", freezeTime);
    }

    void UnFreeze()
    {
        isFrozen = false;
        rbody.velocity = Vector2.right * xDir * speed;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (isFrozen) return;

        if (col.gameObject.tag == "Player")
        {
            print("hit");
            col.gameObject.GetComponent<PlayerController>().ds.Hit(col.contacts[0].point, -damage);

            col.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(xDir, 0.2f) * forceMag);
        }
    }

    void OnDisable()
    {
        CancelInvoke();
    }
}
