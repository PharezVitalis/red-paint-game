﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;


public class PlayerController : MonoBehaviour {

    //Physics
    Rigidbody2D rbody;
    public float skin = 0.1f;
    public float jumpMag = 0.5f;
    [SerializeField]
    LayerMask cMask;

    public float speed = 4;
    Vector2 outputVel;

    [SerializeField]
    SpriteRenderer paintIndicator;

    public int MaxJumps = 2;
  
    int cJumps = 1;

    AudioSource audSrc;

    public int playerNum = 1;
    public float maxHealth = 100;
    public DamageSystem ds;

    GameObject[] weapons;
    IWeapon currentWeapon;
  
    bool canSwap = true;
    int cWeapon = 0;
    bool redMode = true;
    [SerializeField]
    float maxPaint = 200;

    float cPaint;
    bool isAttacking = false;

    bool walkingStepsPlaying = false;

    string player;
   

    GameManager gManager;

    float swapBtnTimer = 0;
    [SerializeField]
    float longPressTime = 0.3f;

    bool facingRight = true;

   
   
    // Use this for initialization
    void Start () {
        gManager = GameManager.current;

        rbody = GetComponent<Rigidbody2D>();

        List<SpriteRenderer> children = new List<SpriteRenderer>();

        children.AddRange(GetComponentsInChildren<SpriteRenderer>());

        jumpMag *= rbody.mass;

        for (int i = children.Count-1; i > -1; i--)
        {
            
            if (children[i].gameObject.tag!= "Overlay")
            {

                children.RemoveAt(i);
            }
            
        }

        ds = new DamageSystem(children.ToArray(), maxHealth,gameObject);

        cPaint = maxPaint;
        InitialiseWeapons();

        audSrc = GetComponent<AudioSource>();

        audSrc.clip = SoundManager.instance.GetSpot("leaf walk");
        audSrc.Play();
    }

    void OnEnable()
    {


        //player = GameManager.current.GetJoyIdentifier(playerNum);

         player = "_p"+playerNum.ToString();
    }
	
	// Update is called once per frame
	void Update () {
        Move();
        RotateX();
        AttackInputs();
        WalkingSounds();
	}

    void Move()
    {


        outputVel = Mover.Move(Input.GetAxisRaw("Horizontal" + player), speed, rbody.velocity.y);
       

       
        if (Input.GetButtonDown("Jump" + player))
        {

            if (Mover.IsGrounded(transform.position, skin, cMask))
            {
                

                Mover.Jump(ref rbody, jumpMag);

                cJumps = 1;

            }
            else if (cJumps < MaxJumps)
            {
              
                Mover.Jump(ref rbody, jumpMag);
                cJumps++;
            }
        }
        else
        {
            if (Mover.IsGrounded(transform.position,skin,cMask)) cJumps = 0;
        }

        

        rbody.velocity = outputVel;
    }

    void RotateX()
    {
      
        
        if ((outputVel.x>0& ! facingRight)|( outputVel.x<0 & facingRight))
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            facingRight = !facingRight;
        }
        
        

    }

    void AttackInputs()
    {

        if (Input.GetButtonDown("Attack" + player))
        {
            if (cPaint > 0)
            {
                isAttacking = true;
                currentWeapon.SetAttacking(true);
            }
        }
        else if (Input.GetButtonUp("Attack" + player))
        {
            isAttacking = false;
            currentWeapon.SetAttacking(false);
        }

        if (Input.GetButtonDown("Swap" + player))
        {
            swapBtnTimer = Time.time;
            canSwap = true;
        }
      else if (Input.GetButton("Swap" + player))
        {
            if((Time.time - swapBtnTimer) > longPressTime & canSwap)
            {
               
                canSwap = false;
                redMode = !redMode;
                currentWeapon.SetRedMode(redMode);

                paintIndicator.color = redMode ? Color.red : Color.green;
               
            }
        }
       else if (Input.GetButtonUp("Swap" + player) & canSwap)
        {
            if(Time.time - swapBtnTimer < longPressTime)
            {
                SwapWeapon();
            }
         
        }


        if (cPaint <= 0 & isAttacking)
        {
            isAttacking = false;
            currentWeapon.SetAttacking(false);
        }
       

        
    }

    void WalkingSounds()
    {
        if (outputVel.x != 0 & !walkingStepsPlaying)
        {
            walkingStepsPlaying = true;
            audSrc.UnPause();
        }
        else if (outputVel.x == 0 & walkingStepsPlaying)
        {
            walkingStepsPlaying = false;
            audSrc.Pause();

        }
    }

    void InitialiseWeapons()
    {
       
        int nOfWeapons = gManager.WeaponTypesSize();
        weapons = new GameObject[nOfWeapons];
      


        for (int i  = 0; i< nOfWeapons; i++)
        {
            weapons[i] = Instantiate(gManager.GetWeapons(i));
            weapons[i].transform.position = new Vector2(transform.position.x, transform.position.y);
            weapons[i].transform.localScale = transform.localScale;
            weapons[i].transform.parent = transform;
            weapons[i].SetActive(false);

        }

        weapons[0].SetActive(true);
        currentWeapon = weapons[0].GetComponent<IWeapon>();

        

     }
       
   void SwapWeapon()
    {
        
        int newWeapon=cWeapon+ 1;

        if (newWeapon == weapons.Length) newWeapon = 0;
      
        if (newWeapon != cWeapon)
        {
            weapons[cWeapon].SetActive(false);
            weapons[newWeapon].SetActive(true);
            cWeapon = newWeapon;

            currentWeapon =  weapons[cWeapon].GetComponent<IWeapon>();
            currentWeapon.SetRedMode(redMode);
            
        }
    }

   public void ChangeInPaint(float amount)
    {

        cPaint = Mathf.Clamp(cPaint + amount, 0, maxPaint);
        paintIndicator.transform.localScale = new Vector3(1, cPaint / maxPaint);
    }

   
}
