﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System;

public class GunWeapon : MonoBehaviour, IWeapon {

    float isInGreenMode = -1; // 1 = true, -1 = false
    GameObject bullet;

    [SerializeField]
    float paintIneffciency = 0.22f;
    float ammoLossPerHit;
    [SerializeField]
    float gunDamage = 10;
   
    [SerializeField]
    string bulletName = "Bullet";

    [SerializeField]
    float bulletGrav = 0;
    [SerializeField]
    float fireRate = 0.4f;

    [SerializeField]
    
    bool isAttacking = false;

    SpriteRenderer tip;

    AudioSource audsrc;

    PlayerController cPlayer;

    // Use this for initialization
    void Awake () {
       
        tip = transform.GetChild(0).GetComponent<SpriteRenderer>();
        audsrc = GetComponent<AudioSource>();

        audsrc.clip = SoundManager.instance.GetSpot("paintball shot");

        ammoLossPerHit = (1 + paintIneffciency) * gunDamage;
    }

    void Start()
    {
        cPlayer = transform.parent.GetComponent<PlayerController>();

    }

    void Fire()
    {
        Bullet b;
       
       
            bullet = Pooler.current.GetPooled(bulletName);

            if (bullet)
            {

                bullet.transform.position = tip.transform.position;
                bullet.transform.rotation = tip.transform.rotation;

                bullet.SetActive(true);

            b = bullet.GetComponent<Bullet>();
            b.Fire(Mathf.Sign(tip.transform.position.x - transform.position.x), bulletGrav,isInGreenMode);

            cPlayer.ChangeInPaint(-ammoLossPerHit);
                
            }
            else return;

        audsrc.Play();
    }
	





    public void SetAttacking(bool isAttacking)
    {
        

        this.isAttacking = isAttacking;
        

        if (isAttacking)
        {
           
            InvokeRepeating("Fire",0, fireRate);
        }
        else
        {
            CancelInvoke();
        }
       
    }

    public void SetRedMode(bool isInRedMode)
    {
        
        if (isInRedMode)
        {
            isInGreenMode = -1;
            tip.color = Color.red;
        }
        else
        {
            isInGreenMode = 1;
            tip.color = Color.green;
        }
    }

    void OnDisable()
    {
        CancelInvoke();
    }
    
}
