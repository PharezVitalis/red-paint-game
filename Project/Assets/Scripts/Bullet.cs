﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class Bullet : MonoBehaviour {

    [SerializeField]
    bool autoFire = false;

    public float speed = 6;

    [SerializeField]
    string decalName = "Decal";

    float damage = 10;
    public float isInGreenMode = -1;

    public float timeOut = 3;
    Rigidbody2D rbody;

    SpriteRenderer rend;

    AudioSource audSrc;

    float xdir;
	// Use this for initialization
	void Awake()
    {
        rbody = GetComponent<Rigidbody2D>();
        audSrc = GetComponent<AudioSource>();
        audSrc.clip = SoundManager.instance.GetSpot("paintbrush splat");

        rend = GetComponent<SpriteRenderer>();
    }
    
	void OnEnable()
    {
        if (autoFire)
        {
            
            rbody.velocity = speed * transform.right;
            Invoke("TimeOut", timeOut);
        }

        rend.color = isInGreenMode > 0 ? Color.green : Color.red;
    }

	// Update is called once per frame
    public void Fire(float dir, float grav,float isInGreenMode )
    {
        if (autoFire)
        {
            return;
        }

        rbody.gravityScale = grav;
        this.isInGreenMode = isInGreenMode;

      

        rbody.velocity = Vector2.right*dir * speed;
        xdir = dir;
        Invoke("TimeOut", timeOut);
    }


    public void Fire(Vector2 dir, float grav, float isInGreenMode)
    {
        rbody.gravityScale = grav;
        this.isInGreenMode = isInGreenMode;

        xdir = dir.x;

        rbody.velocity = dir * speed;

        Invoke("TimeOut", timeOut);
    }


    void TimeOut()
    {
        rbody.velocity = Vector2.zero;
        gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        audSrc.Play();
        if (c.gameObject.tag == "Enemy" )
        {
            c.gameObject.GetComponent<EnemyController>().ds.Hit(c.contacts[0].point, damage*isInGreenMode);
           
        }
        else if(c.gameObject.tag == "Player")
        {
            c.gameObject.GetComponent<PlayerController>().ds.Hit(c.contacts[0].point, damage * isInGreenMode);
        }

      else  if (c.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            GameObject decal = Pooler.current.GetPooled(decalName);
           
            if (decal)
            {
                Vector2 decalDir = new Vector2(xdir, 1);
                

                decal.transform.localScale = decalDir;
                decal.transform.position = transform.position;
                decal.transform.rotation = Quaternion.FromToRotation(Vector3.up, c.contacts[0].normal);
                decal.SetActive(true);
            }
            
        }
        
        CancelInvoke();
        
        rbody.velocity = Vector2.zero;
        gameObject.SetActive(false);
    }
}
