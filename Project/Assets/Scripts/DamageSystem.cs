﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DamageSystem{
   

    float cHealth;
    float maxHealth;

    GameObject target;
    SpriteRenderer[] overlays;
    bool[] activeOverlays;
   
    

    public DamageSystem(SpriteRenderer[] overlays, float maxHealth, GameObject target)
    {
        this.overlays = overlays;
        

        activeOverlays = new bool[overlays.Length];

        for (int i = 0; i < activeOverlays.Length; i++)
        {
            activeOverlays[i] = false;
            
        }

        this.maxHealth = maxHealth;
        cHealth = maxHealth;

        this.target = target;

    
    }

    public void Hit(Vector2 region,float damage)
    {
        
       if (damage > 0|overlays.Length<1)
        {
            HealthChange = damage;
            return;
        }
      

        Vector2 closestPoint= overlays[0].transform.position;
        int closestIndex = 0;

        for (int i = 1; i < overlays.Length; i++)
        {
            if (Vector2.Distance(region, closestPoint) > Vector2.Distance(region, overlays[i].transform.position))
            {
                closestIndex = i;
                closestPoint = overlays[i].transform.position;
            }
        }
        

        activeOverlays[closestIndex] = true;
             HealthChange = damage;
        

    }

    public void Heal(float amount)
    {
        HealthChange = amount;
    }

   void UpdateOpacities()
    {
        if (overlays.Length <1)
            return;

        Color c;
        if (cHealth < maxHealth * .8f)
        {
            for (int i = 0; i < overlays.Length; i++)
            {
                if (activeOverlays[i])
                {
                    c = overlays[i].color;
                    c.a =  1- cHealth / maxHealth;
                    overlays[i].color = c;
                }
            }
        }
        else
        {
            for (int i = 0; i < overlays.Length; i++)
            {
                if (activeOverlays[i])
                {
                    activeOverlays[i] = false;
                    c = overlays[i].color;
                    c.a = 0;
                    overlays[i].color = c;

                }
            }
        }
    }

    public void Reset()
    {
        Color cColor;

        for (int i = 0; i < overlays.Length; i++)
        {
            cColor = overlays[i].color;
            cColor.a = 0;
            overlays[i].color = cColor;
        }

        cHealth = maxHealth;
        

        
    }

    public void Reset(float maxHealth)
    {
        Color cColor;
        for (int i = 0; i < overlays.Length; i++)
        {
            cColor = overlays[i].color;
            cColor.a = 0;
            overlays[i].color = cColor;
        }
        this.maxHealth = maxHealth;
        cHealth = maxHealth;
       

    }


    public float Health
    {
        get
        {
            return cHealth;
        }
       
    }

     float HealthChange
        {
        set
        {
            cHealth += value;

            if (cHealth <=0)
            {
                Reset();
               
                target.SetActive(false);
                return;
            }
            else if (cHealth >maxHealth)
            {
                cHealth = maxHealth;
            }

            UpdateOpacities();


           

        }
    }


   

    
}
