﻿using UnityEngine;
using System.Collections;

public static  class Mover { 

    public static Vector2 Move( float direction, float speed,float yComponent)
    {
        Vector2 outputVel = Vector3.zero;

        outputVel.x = speed * direction;
        outputVel.y = yComponent;


        return outputVel;
    }

    public static bool IsGrounded(Vector2 position,float skin, int cMask)
    {
        bool isGrounded = Physics2D.Raycast(position, Vector2.down, skin, cMask); ;
       
  
        return isGrounded;
    }

    public static void Jump(ref Rigidbody2D rbody, float jumpMag)
    {
       
        rbody.AddForce(Vector2.up * jumpMag);
       
    }

}
