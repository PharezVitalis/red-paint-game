﻿using UnityEngine;
using System.Collections;

public static  class MoverClass { 

    public static Vector2 Move( float direction, float speed,float yComponent)
    {
        Vector2 outputVel = Vector3.zero;

        outputVel.x = speed * direction;
        outputVel.y = yComponent;


        return outputVel;
    }

    public static bool IsGrounded(Vector2 position,float skin, int cMask)
    {
        return Physics2D.Raycast(position, Vector2.down, skin, cMask);
    }

   public static void Jump(Rigidbody2D rbody, float force)
    {
        Vector2 outputVel = rbody.velocity;
        outputVel.y = 0;
        rbody.velocity = outputVel;

        rbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);

       
    }
}
