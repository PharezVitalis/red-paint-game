﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
    Pooler pooler;
    public string type = "cube";
    GameObject tmp;
    public float delay = 1;

	// Use this for initialization
	void Start () {
        pooler = Pooler.current;
        InvokeRepeating("SpawnWave", delay, delay);
	}
	
	// Update is called once per frame
	void SpawnWave()
    {

        tmp = pooler.GetPooled(type);
        if (tmp)
        {
            tmp.transform.position = transform.position;
            tmp.SetActive(true);
        }
    }
}
