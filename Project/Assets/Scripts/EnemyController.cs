﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EnemyController : MonoBehaviour
{
    [SerializeField]
    public float maxHealth = 100;
   public DamageSystem ds;
    [SerializeField]
   

   

    void Awake()
    {
       List<SpriteRenderer> children = new List<SpriteRenderer>();

        children.AddRange(gameObject.GetComponentsInChildren<SpriteRenderer>());
       
        for (int i = children.Count - 1; i >-1; i--)
        {
           

            if (children[i].gameObject.tag != "Overlay")
            {
                children.RemoveAt(i);
                
                
            }
        }

        ds = new DamageSystem(children.ToArray(), maxHealth, gameObject);
    }


    void OnEnable()
    {
        ds.Reset();
    }

    void OnDisable()
    {
        LevelManager.instance.EnemyDeath();
    }
    


}
