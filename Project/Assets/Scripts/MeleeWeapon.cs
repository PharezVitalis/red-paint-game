﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class MeleeWeapon:MonoBehaviour,IWeapon {
    bool isAttacking = false;

    [SerializeField]
    float paintIneffciency = 0.1f;
    float paintLossPerHit;

    float inGreenMode = -1; // 1 = true, -1  = false
    [SerializeField]
    float attackRate = 0.7f;
    float timeAtLastAttack=0;
    Animator anim;
    SpriteRenderer modeIndicator;

    [SerializeField]
    float damage = 20;
    
    Vector3 tipPos;
    AudioSource[] audSrc;
   


    PlayerController cPlayer;

    void Awake()
    {
        audSrc = GetComponents<AudioSource>();
        audSrc[0].clip = SoundManager.instance.GetSpot("paintbrush swish");
        audSrc[1].clip = SoundManager.instance.GetSpot("paintbrush splat");

        anim = gameObject.GetComponent<Animator>();
        modeIndicator = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void Start()
    {
       

        cPlayer = transform.parent.GetComponent<PlayerController>();

        paintLossPerHit = damage * (1 + paintIneffciency);

        tipPos.x = 0.31f;
        tipPos.y = 0.58f;
    }

   public void SetAttacking(bool isAttacking)
    {
        if (isAttacking) this.isAttacking = isAttacking;
        else Invoke("AttackingTimeOut", attackRate*0.8f);
        
            anim.SetBool("isAttacking", isAttacking);
        
    }

    public void SetRedMode(bool isInRedMode)
    {
       if (isInRedMode)
        {
            inGreenMode = -1;
            modeIndicator.color = Color.red;
        }
        else
        {
            inGreenMode = 1;
            modeIndicator.color = Color.green;
        }
    }
	
   
    void OnTriggerStay2D(Collider2D c)
    {
        if (isAttacking && Time.time - timeAtLastAttack > attackRate)
        {
            
            if (c.gameObject.tag == "Enemy")
            {
                audSrc[0].Play();

                timeAtLastAttack = Time.time;
                c.GetComponent<EnemyController>().ds.Hit(transform.position + tipPos, inGreenMode * damage);
                cPlayer.ChangeInPaint(paintLossPerHit);
            }
            else if (c.gameObject.tag == "Player" & inGreenMode == 1)
            {
                audSrc[0].Play();

                timeAtLastAttack = Time.time;
                c.GetComponent<PlayerController>().ds.Heal(damage);
                cPlayer.ChangeInPaint(paintLossPerHit);
            }

        }
    }

    void AttackingTimeOut()
    {
        isAttacking = false;
    }
    
}

public interface IWeapon
{
   void SetAttacking(bool isAttacking);
    void SetRedMode(bool isInRedMode);

   

}
