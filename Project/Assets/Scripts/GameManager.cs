﻿using UnityEngine;
using System.Collections;


public class GameManager : MonoBehaviour {

    public static GameManager current;
     
    string[] players = new string[4];

    [SerializeField]
    GameObject[] weaponTypes;

    GameObject[] activePlayers;
    int nOfPlayers = 0;

	
	void Awake () {
        if (!current)
        {
            current = this;
        }
        else
        {
            Destroy(gameObject);
        }
	}
	
    public GameObject GetWeapons(int index)
    {
        return weaponTypes[index];
    }

    public int WeaponTypesSize()
    {
        return weaponTypes.Length;
    }

    public string GetJoyIdentifier(int player)
    {
      if (players == null)
        {
            return null;
        }
        else
        {
            return players[player - 1];
        }
    }

    public void AssignPlayer (int joyNum)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i]== null)
            {
                players[i] = "_p" + joyNum.ToString();
                nOfPlayers++;
                return;
            }
        }
    }

    public void DropOut(int player)
    {
        if (player == 1) return;
        player--;
        nOfPlayers--;
        players[player] = null;

    }
   
    public int GetPlayerCount
    {
        get
        {
            return nOfPlayers;
        }
    }
   
	
}
