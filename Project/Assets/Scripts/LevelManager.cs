﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class LevelManager : MonoBehaviour {

    [SerializeField]
    private string ambienceName;
    [SerializeField]
    private string musicName;

    [SerializeField]
    AudioSource ambience;
    [SerializeField]
    AudioSource music;

    [SerializeField]
    int waveCount = 5;
    int cWave = 1;
    [SerializeField]
    int initialEnemyPerWave = 4;

    [SerializeField]
    float waveMultiplier = 1.2f;

    [SerializeField]
    string[] enemyTypes;
    GameObject[,] enemyWave;

    [SerializeField]
    GameObject spawnerParent;
    Transform[] spawners;

    public static LevelManager instance;

    int enemiesRemaining;

	// Use this for initialization
	void Start () {
        ambience.clip = SoundManager.instance.GetAmbience(ambienceName);
        ambience.Play();

        music.clip = SoundManager.instance.GetMusic(musicName);
        music.Play();

       

        spawners = spawnerParent.GetComponentsInChildren<Transform>();

        instance = this;

        StartCoroutine(SpawnWave());

        
	}
	
    IEnumerator SpawnWave()
    {
        int enemyToSpawn = Mathf.RoundToInt(waveMultiplier * (float)cWave) * initialEnemyPerWave;
        int cEnemyType = 0;
        int cSpawnLocation = 0;
        

        enemiesRemaining = enemyToSpawn;

        enemyWave = new GameObject[enemyTypes.Length, enemyToSpawn];


        enemyToSpawn--;
        while (enemyToSpawn >-1)
        {
            enemyWave[cEnemyType, enemyToSpawn] = Pooler.current.GetPooled(enemyTypes[cEnemyType]);
            if (enemyWave[cEnemyType, enemyToSpawn])
            {
                
                enemyWave[cEnemyType, enemyToSpawn].transform.position = spawners[cSpawnLocation].position;

                enemyWave[cEnemyType, enemyToSpawn].SetActive(true);
                enemyToSpawn--;
            }
            yield return new WaitForSeconds(0.2f);
            cEnemyType++;
            cSpawnLocation++;
            if (cEnemyType >= enemyTypes.Length) cEnemyType = 0;
            if (cSpawnLocation >= spawners.Length) cSpawnLocation = 0;
        }
    }

    public void EnemyDeath()
    {
        enemiesRemaining--;
        if (enemiesRemaining <= 0)
        {
            cWave++;

           
        }

        StartCoroutine(SpawnWave());
    }

    
	
	
}
