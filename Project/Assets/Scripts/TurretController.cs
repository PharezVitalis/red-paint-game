﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System.Collections.Generic;

public class TurretController :MonoBehaviour{
    [SerializeField]
    string bulletName = "Bullet";
    [SerializeField]
    float burstFireRate = 0.3f;
    [SerializeField]
    float amountToFire =3;

    [SerializeField]
    float turretFireTime = 3;

    AudioSource audSrc;

    [SerializeField]
    float sightRadius = 3;

    [SerializeField]
    SpriteRenderer tip;

    float isInGreenMode = -1;

    [SerializeField]
    LayerMask validTargets;

   

    Transform target;

    [SerializeField]
    Transform bulletSpawn;

    [SerializeField]
    float minDist = 0.2f;

    Quaternion lookRotation;


    float retargetTime = 1f;
   
    
	// Use this for initialization
	void Start () {
        retargetTime += Random.Range(-0.15f, 0.15f);// prevents all the turrets from retargetting at once if there are mutiple on scene

        InvokeRepeating("GetTarget", 0, retargetTime);
        StartCoroutine(Fire());
        audSrc = GetComponent<AudioSource>();
        audSrc.clip = SoundManager.instance.GetSpot("paintball shot");

       
	}
	
	// Update is called once per frame
	 void GetTarget()
    {
        // returns a target using a circle cast to see what character objects are within it's LOS

      RaycastHit2D[] hitInfo =  Physics2D.CircleCastAll(transform.position, sightRadius, Vector2.right, 0, validTargets);
      
        if (hitInfo.Length < 2)
        {
           
            target = null;
            return;
        }

        bool[] isPlayer = new bool[hitInfo.Length];
        bool aPlayerIsInRange = false;

        int newTargetIndex = 0; // the array element in hitInfo that will be set as the target

        // used for distance calculations in the for loop
        float dist = 100000;
        float newDist = 0;

        for (int i = 0; i < hitInfo.Length; i++)
        {// maps out which hits where players and which were enemies
            if (hitInfo[i].collider.tag == "Player")
            {
                isPlayer[i] = true;
                aPlayerIsInRange = true;
            }
            else
            {
                isPlayer[i] = false;
            }
        }
      
            
            for (int i = 0; i < hitInfo.Length; i++)
            { 
                if (isPlayer[i] != aPlayerIsInRange) // turret will prioritise firing at the player
            {
                continue;
            }
                

                newDist = Vector2.Distance(transform.position, hitInfo[i].transform.position); // distance between array element and turrret

                 if (newDist < dist & newDist>minDist)
                {
                    dist = newDist;
                    newTargetIndex = i;
                }

            }

        IsInGreenMode = aPlayerIsInRange ? -1 : 1; // if a player is in range then it will deal damage to the player (and deliberately to other targets)

        target = hitInfo[newTargetIndex].transform;
    }

    void OnDisable()
    {
        CancelInvoke();
        StopAllCoroutines();
    }

    int IsInGreenMode
    {
        set
        {
            isInGreenMode = value;
            if (isInGreenMode == 1)
            {
                tip.color = Color.green;
            }
            else
            {
                tip.color = Color.red;
            }
        }
    }

    IEnumerator Fire()
    {
        print("entered");
        yield return new WaitForSeconds(turretFireTime);
      
            while(target == null)
        {
            yield return new WaitForFixedUpdate();
        }
        
        for (int i = 0; i < amountToFire; i++)
        {
            
            GameObject newBullet = Pooler.current.GetPooled(bulletName);
            newBullet.transform.position = bulletSpawn.transform.position;
            newBullet.transform.rotation = bulletSpawn.transform.rotation;
            newBullet.SetActive(true);
            newBullet.GetComponent<Bullet>().isInGreenMode = isInGreenMode;
            audSrc.Play();
            yield return new WaitForSeconds(burstFireRate);

        }

        StartCoroutine(Fire());
    }

    void Update()
    {

        if (!target) return;

            RotateView();
    

    }

    void RotateView()
    {

        


            Vector3 diff = target.position - transform.position;
            diff.Normalize();



            lookRotation = Quaternion.LookRotation(diff, transform.TransformDirection(Vector3.up));

            transform.rotation = new Quaternion(0, 0, lookRotation.z, lookRotation.w);
        
        
    }
   
  


}
